using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using DeadLinkDetector.Scanner;
using DeadLinkDetector.Scanner.Io;

namespace DeadLinkDetector.Scanner.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestFunction()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            var result = function.FunctionHandler(new ScanRequest{RootUrl = "https://readify.net/", MaxDepth = 1}, context).GetAwaiter().GetResult();
        }
    }
}
