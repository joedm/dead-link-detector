﻿using System.Collections.Generic;
using System.Linq;
using DeadLinkDetector.Scanner.Logic.Scan.Poco;
using SmallCode.AspNetCore.HtmlAgilityPack;

namespace DeadLinkDetector.Scanner.Logic.Scan
{
    public class LinkExtractor : ILinkExtractor
    {
        public PageLinks GetAllLinks(HtmlNode node)
        {
            return new PageLinks
            {
                Anchors = GetAnchorLinks(node),
                Images = GetImageLinks(node)
            };
        }

        private IEnumerable<string> GetAnchorLinks(HtmlNode node)
        {
            return node.SelectNodes("//a[@href]").Select(x => x.GetAttributeValue("href", string.Empty));
        }

        private IEnumerable<string> GetImageLinks(HtmlNode node)
        {
            return node.SelectNodes("//img[@src]").Select(x => x.GetAttributeValue("src", string.Empty));
        }
    }
}