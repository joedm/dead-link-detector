﻿using DeadLinkDetector.Scanner.Logic.Scan.Poco;
using SmallCode.AspNetCore.HtmlAgilityPack;

namespace DeadLinkDetector.Scanner.Logic.Scan
{
    public interface ILinkExtractor
    {
        PageLinks GetAllLinks(HtmlNode node);
    }
}