﻿using System.Collections.Generic;

namespace DeadLinkDetector.Scanner.Logic.Scan.Poco
{
    public class PageLinks
    {
        public IEnumerable<string> Anchors { get; set; }
        public IEnumerable<string> Images { get; set; }
    }
}