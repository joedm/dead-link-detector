﻿using System.Threading.Tasks;
using DeadLinkDetector.Scanner.Logic.Scan.Poco;

namespace DeadLinkDetector.Scanner.Logic.Scan
{
    public interface IScanner
    {
        Task<PageLinks> FindAllLinks(string rootUrl, int maxDepth);
    }
}