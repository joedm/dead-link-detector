﻿using System;
using System.Threading.Tasks;
using DeadLinkDetector.Scanner.Logic.Scan.Poco;
using SmallCode.AspNetCore.HtmlAgilityPack;
using Thinktecture.Net.Http;
using Thinktecture.Net.Http.Adapters;

namespace DeadLinkDetector.Scanner.Logic.Scan
{
    public class Scanner : IScanner, IDisposable
    {
        private readonly ILinkExtractor _linkExtractor;
        private readonly IHttpClient _httpClient;

        public Scanner(IHttpClient httpClient, ILinkExtractor linkExtractor)
        {
            _linkExtractor = linkExtractor ?? new LinkExtractor();
            _httpClient = httpClient ?? new HttpClientAdapter();
        }

        public async Task<PageLinks> FindAllLinks(string rootUrl, int maxDepth)
        {
            string html = await _httpClient.GetStringAsync(rootUrl).ConfigureAwait(false);
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            return _linkExtractor.GetAllLinks(doc.DocumentNode);
        }

        public void Dispose()
        {
            _httpClient?.Dispose();
        }
    }
}