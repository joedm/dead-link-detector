﻿namespace DeadLinkDetector.Scanner.Io
{
    public class ScanRequest
    {
        public string RootUrl { get; set; }
        public int MaxDepth { get; set; }
    }
}