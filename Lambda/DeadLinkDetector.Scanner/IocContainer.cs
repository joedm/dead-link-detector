﻿using System;
using Autofac;
using DeadLinkDetector.Scanner.Logic.Scan;
using Thinktecture.Net.Http;
using Thinktecture.Net.Http.Adapters;

namespace DeadLinkDetector.Scanner
{
    public class IocContainer
    {
        #region Singleton Pattern
        private static readonly Lazy<IocContainer> LazyIoc = new Lazy<IocContainer>(() => new IocContainer());

        public static IocContainer Instance => LazyIoc.Value;

        private IocContainer()
        {
            Container = BuildContainer();
        }
        #endregion

        public IContainer Container { get; }

        private IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<HttpClientAdapter>().As<IHttpClient>().InstancePerDependency();
            builder.RegisterType<Logic.Scan.Scanner>().As<IScanner>().InstancePerLifetimeScope();
            builder.RegisterType<LinkExtractor>().As<ILinkExtractor>().SingleInstance();
            return builder.Build();
        }
    }
}