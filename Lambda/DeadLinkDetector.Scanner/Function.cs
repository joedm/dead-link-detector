using System;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Autofac;
using DeadLinkDetector.Scanner.Io;
using DeadLinkDetector.Scanner.Logic.Scan;
using DeadLinkDetector.Scanner.Logic.Scan.Poco;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace DeadLinkDetector.Scanner
{
    public class Function : IDisposable
    {
        private readonly ILifetimeScope _lifetimeScope;
        public IScanner Scanner { get; set; }

        public Function()
        {
            _lifetimeScope = IocContainer.Instance.Container.BeginLifetimeScope();
            _lifetimeScope.InjectUnsetProperties(this);
        }

        public Function(IScanner scanner)
        {
            Scanner = scanner;
        }

        public Task<PageLinks> FunctionHandler(ScanRequest request, ILambdaContext context)
        {
            context.Logger.LogLine($"Request Payload: {JsonConvert.SerializeObject(request)}");
            
            return Scanner.FindAllLinks(request.RootUrl, request.MaxDepth);
        }

        public void Dispose()
        {
            _lifetimeScope?.Dispose();
        }
    }
}
